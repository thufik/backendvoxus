--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: tasks_id_sequence; Type: SEQUENCE; Schema: public; Owner: Thufik
--

CREATE SEQUENCE tasks_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tasks_id_sequence OWNER TO "Thufik";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tasks; Type: TABLE; Schema: public; Owner: Thufik
--

CREATE TABLE tasks (
    id integer DEFAULT nextval('tasks_id_sequence'::regclass) NOT NULL,
    title character varying(40),
    description character varying(40),
    priority integer,
    state text,
    created_at date,
    user_id integer
);


ALTER TABLE tasks OWNER TO "Thufik";

--
-- Name: user_id_sequence; Type: SEQUENCE; Schema: public; Owner: Thufik
--

CREATE SEQUENCE user_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_sequence OWNER TO "Thufik";

--
-- Name: voxus_user_id_sequence; Type: SEQUENCE; Schema: public; Owner: Thufik
--

CREATE SEQUENCE voxus_user_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE voxus_user_id_sequence OWNER TO "Thufik";

--
-- Name: voxus_user; Type: TABLE; Schema: public; Owner: Thufik
--

CREATE TABLE voxus_user (
    name text,
    id integer DEFAULT nextval('voxus_user_id_sequence'::regclass) NOT NULL,
    email text
);


ALTER TABLE voxus_user OWNER TO "Thufik";

--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: Thufik
--

COPY tasks (id, title, description, priority, state, created_at, user_id) FROM stdin;
28	y	x	4	done	2017-11-17	3
7	Teste	TESTANDO	5	done	2017-11-17	3
33	Aaa	Aaaa	5	sended	2017-11-17	3
27	Task x	Task tal	5	sended	2017-11-17	3
13	1833	1833	1	done	2017-11-17	3
34	SALVANDO	SALVANDO	5	done	2017-11-17	3
31	Dsada	Dasdas	5	done	2017-11-19	3
26	18e53	18e53	3	done	2017-11-19	3
15	1837	1837	1	sended	2017-11-17	3
16	Teste	Teste	1	sended	2017-11-17	3
17	1839	1839	1	sended	2017-11-17	3
18	1840	1840	1	sended	2017-11-17	3
19	Teste	1844	1	sended	2017-11-17	3
20	1844	1844	1	sended	2017-11-17	3
21	1846	1846	1	sended	2017-11-17	3
22	1847	1847	1	sended	2017-11-17	3
23	18e47	18e47	1	sended	2017-11-17	3
24	Dasdsa	Dasdas	1	sended	2017-11-17	3
25	1852	1852	2	sended	2017-11-17	3
35	17e56	Dasds	1	done	2017-11-19	3
\.


--
-- Name: tasks_id_sequence; Type: SEQUENCE SET; Schema: public; Owner: Thufik
--

SELECT pg_catalog.setval('tasks_id_sequence', 35, true);


--
-- Name: user_id_sequence; Type: SEQUENCE SET; Schema: public; Owner: Thufik
--

SELECT pg_catalog.setval('user_id_sequence', 1, false);


--
-- Data for Name: voxus_user; Type: TABLE DATA; Schema: public; Owner: Thufik
--

COPY voxus_user (name, id, email) FROM stdin;
teste	1	teste@teste.com
hue	2	hue@teste.com
Fellipe Thufik Costa Gomes Hoashi	3	thufik7@gmail.com
testando	4	hue
testando	5	huedsadsad
testando	6	dsdasdasdsadasdsadsa
testando	7	s
\.


--
-- Name: voxus_user_id_sequence; Type: SEQUENCE SET; Schema: public; Owner: Thufik
--

SELECT pg_catalog.setval('voxus_user_id_sequence', 7, true);


--
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: Thufik
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: voxus_user voxus_user_pkey; Type: CONSTRAINT; Schema: public; Owner: Thufik
--

ALTER TABLE ONLY voxus_user
    ADD CONSTRAINT voxus_user_pkey PRIMARY KEY (id);


--
-- Name: tasks user_id; Type: FK CONSTRAINT; Schema: public; Owner: Thufik
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES voxus_user(id);


--
-- Name: tasks_id_sequence; Type: ACL; Schema: public; Owner: Thufik
--

GRANT SELECT,USAGE ON SEQUENCE tasks_id_sequence TO username;


--
-- Name: tasks; Type: ACL; Schema: public; Owner: Thufik
--

GRANT ALL ON TABLE tasks TO username;


--
-- Name: user_id_sequence; Type: ACL; Schema: public; Owner: Thufik
--

GRANT ALL ON SEQUENCE user_id_sequence TO username;


--
-- Name: voxus_user_id_sequence; Type: ACL; Schema: public; Owner: Thufik
--

GRANT SELECT,USAGE ON SEQUENCE voxus_user_id_sequence TO username;


--
-- Name: voxus_user; Type: ACL; Schema: public; Owner: Thufik
--

GRANT ALL ON TABLE voxus_user TO username;


--
-- PostgreSQL database dump complete
--


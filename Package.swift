import PackageDescription

let package = Package(
    name: "BackendVoxus",
    dependencies: [
        .Package(
        url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git",majorVersion: 2),
        .Package(url: "https://github.com/SwiftORM/Postgres-StORM.git", majorVersion: 1)
    ]
)

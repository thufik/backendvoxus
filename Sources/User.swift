//
//  User.swift
//  BackendVoxusPackageDescription
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 12/11/17.
//

import PostgresStORM
import StORM

class User: PostgresStORM {
    var id : Int = 0
    var name : String = ""
    var email : String = ""
    
    override open func table() -> String {
        return "voxus_user"
    }
    
    override func to(_ this: StORMRow) {
        id = this.data["id"] as? Int ?? 0
        name = this.data["name"] as? String ?? ""
        email = this.data["email"] as? String ?? ""
    }
    
    func rows() -> [User] {
        var rows = [User]()
        for i in 0..<self.results.rows.count {
            let row = User()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
}

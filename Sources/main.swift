import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import PostgresStORM
import PostgreSQL
import Foundation




// Create HTTP server.
let server = HTTPServer()

// Register your own routes and handlers
var routes = Routes()

routes.add(method : .post, uri: "/", handler:{ request, response in
    let success = ["message" : "Ok"]
    
    response.appendBody(string: try! success.jsonEncodedString())
    
    response.completed()
})

routes.add(method : .post, uri: "/task", handler: {
    request, response in

        let params = request.params()
    
        let decoded = try! params[0].0.jsonDecode() as! [String : Any]
    
        let task = Task()
    
        task.title = decoded["title"] as! String
        task.description = decoded["description"] as! String
        task.priority = decoded["priority"] as! Int
        task.state = decoded["state"] as! String
        task.user_id = decoded["user_id"] as! Int
    
        task.created_at = NSDate()
    
        do{
            try! task.save()
        }catch{
            print("houve o erro \(error)")
        }
    
        let success = ["message" : "Ok"]
    
        response.appendBody(string: try! success.jsonEncodedString())
    
        response.completed()
    }
)

routes.add(method: .get, uri: "/tasks", handler: {
    request, response in

        let task = Task()
    
        try! task.findAll()

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
    
        var dic : [[String : Any]] = []
    
        for i in 0..<task.rows().count{
            dic.append(task.rows()[i].asDataDict())
            dic[i]["created_at"] = formatter.string(from: dic[i]["created_at"] as! Date)
        }
    
        response.appendBody(string: try! dic.jsonEncodedString())
        response.completed()
    }
)

routes.add(method: .delete, uri: "/task/{id}", handler: {
    
    request, response in
    
    let task = Task()
    
    let Optionid = request.urlVariables["id"]
    
    if let id = Optionid{
        try! task.delete(Int(id)!)
    }

    let success = ["message" : "Ok"]
    
    response.appendBody(string: try! success.jsonEncodedString())
    
    response.completed()
})

routes.add(method: .put, uri: "/task/{id}", handler: {
    
    request, response in
    
        let Optionid = request.urlVariables["id"]
    
        let params = request.params()
    
        let decoded = try! params[0].0.jsonDecode() as! [String : Any]
    
        let task = Task()

        if let id = Optionid{
            try! task.get(id)
        }
    
        if let state = decoded["state"] as? TaskState.RawValue{
            task.state = state
        }
    
        if let title = decoded["titile"] as? String{
            task.title = title
        }
    
        if let description = decoded["description"] as? String{
            task.description = description
        }
    
        if let priority = decoded["priority"] as? Int{
            task.priority = priority
        }
    
        do{
            try! task.save()
        }catch{
            print("houve o erro \(error)")
        }
    
        let responseJson = ["message" : "Ok"]
    
        response.appendBody(string: try! responseJson.jsonEncodedString())
        response.completed()
})

routes.add(method: .post, uri: "/login", handler: {
    request, response in
    
    let params = request.params()
    
    let decoded = try! params[0].0.jsonDecode() as! [String : Any]
    
    let email = decoded["email"] as! String
    
    let name = decoded["name"] as! String
    
    let user = User()
    
    user.email = email
    user.name = name
    
    try! user.find(["email" : email])

    if user.rows().count == 0{

        user.name = name
        user.email = email

        try! user.save {
            id in user.id = id as! Int
        }
    }
    
    let responseJson = ["user" : user.asDataDict()]
    
    print(responseJson)
    
    response.appendBody(string: try! responseJson.jsonEncodedString())
    response.completed()
})


routes.add(method: .post, uri: "/upload", handler: {
    request, response in
    
    if let uploads = request.postFileUploads{
        print("Tem upload archives")
        
        for upload in uploads{
            
        }
        
        print(Dir.workingDir.path)
        
        let fileDir = Dir(Dir.workingDir.path + "files")
        
        do{
            try fileDir.create()
        }catch{
            print(error)
        }
        
    }else{
        print("sem")
    }
    
    response.completed()
    
})

// Add the routes to the server.
server.addRoutes(routes)

// Set a listen port of 8181
server.serverPort = 8181

do {
    // Launch the HTTP server.
    
    PostgresConnector.host        = "localhost"
    PostgresConnector.username    = "username"
    PostgresConnector.password    = "secret"
    PostgresConnector.database    = "yourdatabase"
    PostgresConnector.port        = 5432

    _ = PostgresConnect.init(host: "localhost", username: "username", password: "secret", database: "yourdatabase", port: 5432)

    let p = PGConnection()

    let status = p.connectdb("host=localhost dbname=postgres")

    print(status)
    
    try server.start()
    
} catch PerfectError.networkError(let err, let msg) {
    print("Network error thrown: \(err) \(msg)")
}


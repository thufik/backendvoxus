//
//  Task.swift
//  BackendVoxusPackageDescription
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 08/11/17.
//

import Foundation
import StORM
import PostgresStORM

public enum TaskState : String{
    case sended = "sended"
    case processed = "processed"
    case done = "done"
}

class Task: PostgresStORM {
    var id : Int = 0
    var title : String = ""
    var description : String = ""
    var priority : Int = 1
    var state : String = TaskState.sended.rawValue
    var created_at : NSDate = NSDate()
    var user_id : Int = 0

    override open func table() -> String {
        return "tasks"
    }
    
    override func to(_ this: StORMRow) {
        id = this.data["id"] as? Int ?? 0
        title = this.data["title"] as? String ?? ""
        description = this.data["description"] as? String ?? ""
        priority = this.data["priority"] as? Int ?? 1
        state = this.data["state"] as? String ?? TaskState.sended.rawValue
        created_at = this.data["created_at"] as? NSDate ?? NSDate()
        user_id = this.data["user_id"] as? Int ?? 0
    }
    
    func rows() -> [Task] {
        var rows = [Task]()
        for i in 0..<self.results.rows.count {
            let row = Task()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }

}


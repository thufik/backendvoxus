# README #

Esse README é referente ao Backend do desafio da Voxus

### Resumo geral ###

Essa parte do desafio foi desenvolvida também em Swift, em conjunto com o framework [Perfect](http://perfect.org/)
A etapa mais custosa foi essa, sem dúvidas. Gastei cerca de 70 a 80 por cento do tempo no backend.A linguagem Swift e o encerramento de um projeto onde eu não precisava 
me preocupar com o back-end me motivaram a começar os estudos utilizando o Perfect. Ah, [esse benchmark](https://medium.com/@rymcol/benchmarks-for-the-top-server-side-swift-frameworks-vs-node-js-24460cfe0beb) também me animou :)
Enfim, venho estudando sobre esse framework há alguns meses, antes mesmo de ter ciência sobre esse desafio. Somente testes básicos.

Recentemente tive algumas experiências com linguagens que são costumeiramente utilizadas no backend, como PHP, C# e Python, mas eu nunca  
havia criado uma API. A criação de rotas utilizando o framework é ABSURDAMENTE abstraído e simples de implementar. A criação de uma rota e subir server em localhost
demora em torno de 5 minutos. 

Um dos primeiros problemas enfrentados foi a criação da base de dados utilizando Swift, Perfect e Postgres. Nunca havia utilizado o Postgres e tive 
uma certa curva de aprendizado para conseguir ceder os privilégios, criação do database e das tabelas. Perdi algum tempo usando CLI também.
Desnecessário. Deveria ter optado por uma GUI de cara. Esse [tutorial](https://www.codementor.io/devops/tutorial/getting-started-postgresql-server-mac-osx)
foi de grande ajuda. 

Por fim, tentei publicar o código no Heroku e tive uma série de dificuldades. Eu escolhi o Heroku pois já havia subido aplicações com ele.

O primeiro problema é que o heroku não possui um tutorial de integração com Swift, assim como possui para PHP, NODE.JS e etc, o que já dificulta um pouco.

Segundo, foi MUITO complicado fazer o Setup inicial da aplicação. Demorei tempo até descobrir que tinha que utilizar um [buildpack](https://github.com/kylef/heroku-buildpack-swift)
Depois descobri que havia problemas com a versão do Swift, e por fim, não consegui subir o back-end. Há um problema em relação a porta que a aplicação utiliza e a porta que o heroku sobe o webserver que eu não consegui solucionar.
Resolvi dar uma olhada em algum outro framework web para Swift. Optei pelo [vapor](https://vapor.codes/). Aparentemente a comunidade evoluiu
esse framework com uma maior velocidade, ele parece ser mais estável e possui muito menos linhas de código para subir uma rota.
Consegui subir rapidamente um "Hello-World" no heroku mas resolvi não migrar todo o código que eu já havia escrito utilizando o Perfect
para vapor. 

Esse desafio foi muito importante como um gatilho para eu mergulhar os estudos utilizando back-end em swift e desenvolver Apps por conta própria.

Das 5 etapas, acredito que só a quinta não foi finalizada. 

### Como subir o ambiente ###


## Pré reqs ##

1. Ter o postgres instalado na máquina.

## Subindo o server

1. Executar o comando `swift build` na pasta do BackendVoxus.

2. Depois que terminar de compilar tudo, executar o comando `swift package generate-xcodeproj` para gerar um projeto no xcode.

3. Abra o main.swift do projeto e altere o nome de usuário e o nome do banco de dados, para os dados que você utilizará localmente.

4. Crie um novo DB no postgres CREATE DATABASE NOME_QUALQUER; e use o comando `psql -d (NOME_QUALQUER) -f db.sql` para importar o modelo que eu criei

5. Execute o projeto em swift com .build/debug/BackendVoxus no terminal ou pelo xcode e selecionando o último schema.





